FROM perl:5.30.0
LABEL maintainer="ialoneambest@gmail.com"

# https://rt.cpan.org/Public/Bug/Display.html?id=122802
RUN apt-get update ; apt-get -y install libssl-dev
RUN cpanm Carton Starman 

COPY . /tls-check
RUN cd /tls-check && carton install --deployment

WORKDIR /tls-check
EXPOSE 3000
CMD carton exec starman --port 3000 bin/app.psgi
